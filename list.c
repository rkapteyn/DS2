
#include <stdio.h>
#include <stdlib.h>
#include "list.h"

struct node {
    int data;
    struct node *next;
    struct node *prev;
};

struct list {
    struct node *head;
    struct node *tail;
    int length;
};

struct list* list_init() {
    struct list *l = malloc(sizeof(struct list));
    l->head = NULL;
    l->length = 0;
    return l;
}

int list_cleanup(struct list *l) {
    struct node *n1, *n2;
    n1 = l->head;
    for (int i = 0; i < l->length; i++) {
        n2 = n1->next;
        free(n1);
        n1 = n2;
    }
    free(l);
    return 0;
}

struct node* list_new_node(int num) {
    struct node *n = malloc(sizeof(struct node));
    n->data = num;
    n->next = NULL;
    n->prev = NULL;
    return n;
}

int list_add(struct list *l, int num) {
    struct node *n = list_new_node(num);
    n->next = l->head;
    if (l->head != NULL) l->head->prev = n;
    l->head = n;
    l->length++;
    return 0;
}

int list_add_back(struct list *l, int num) {
    // ... SOME CODE MISSING HERE ...
}

struct node* list_head(struct list *l) {
    if (l->length == 0) return NULL; else return l->head;
}

int list_length(struct list *l) {
    return l->length;
}

int list_node_data(struct node* n) {
    // ... SOME CODE MISSING HERE ...
}

struct node* list_next(struct node* n) {
    // ... SOME CODE MISSING HERE ...
}

struct node* list_prev(struct list* l, struct node* n) {
    // ... SOME CODE MISSING HERE ...
}

int list_unlink_node(struct list* l, struct node* n) {
    // ... SOME CODE MISSING HERE ...
}

void list_free_node(struct node* n) {
    // ... SOME CODE MISSING HERE ...
}

int list_insert_after(struct list* l, struct node* n, struct node* m) {
    // ... SOME CODE MISSING HERE ...
}

int list_insert_before(struct list* l, struct node* n, struct node* m) {
    // ... SOME CODE MISSING HERE ...
}
